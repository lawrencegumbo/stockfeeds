// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyAL1IIvRaiyJIxqXPKUfIuE7bHP4CyOnEs",
    authDomain: "reino-17527.firebaseapp.com",
    databaseURL: "https://reino-17527.firebaseio.com",
    projectId: "reino-17527",
    storageBucket: "reino-17527.appspot.com",
    messagingSenderId: "970626293511",
    appId: "1:970626293511:web:a5c0522ac889f4ffb36fa2",
    measurementId: "G-S7J3RL8H2G"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

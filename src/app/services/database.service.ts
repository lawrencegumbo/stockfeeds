import { Platform } from '@ionic/angular';
import { Injectable } from '@angular/core';
import { SQLitePorter } from '@ionic-native/sqlite-porter/ngx';
import { HttpClient } from '@angular/common/http';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';
import { BehaviorSubject, Observable } from 'rxjs';


export interface Dev {
  poultryId: number,
  poultryName: string,
  poultryCount: number,
  poultryType: number
  poultryDate: string,
  poultryNotes: string
}


@Injectable({
  providedIn: 'root'
})
export class DatabaseService {

  private database: SQLiteObject;
  private dbReady: BehaviorSubject<boolean> = new BehaviorSubject(false);
 
  poultry = new BehaviorSubject([]);
  consumption = new BehaviorSubject([]);
  expenses = new BehaviorSubject([]);
  feed = new BehaviorSubject([]);
  light = new BehaviorSubject([]);
  medication = new BehaviorSubject([]);
  meds = new BehaviorSubject([]);
  mortality = new BehaviorSubject([]);
  phase = new BehaviorSubject([]);
  poultryType = new BehaviorSubject([]);
  sold = new BehaviorSubject([]);
  temperature = new BehaviorSubject([]);
  weight = new BehaviorSubject([]);


  constructor(private plt: Platform, private sqlitePorter: SQLitePorter, private sqlite: SQLite, private http: HttpClient) {
    this.plt.ready().then(() => {
      this.sqlite.create({
        name: 'stockfeeds.db',
        location: 'default'
      })
      .then((db: SQLiteObject) => {
          this.database = db;
          this.poultryDatabase();
      });
    });
  }

  poultryDatabase() {
    this.http.get('assets/database/poutry.sql', { responseType: 'text'})
    .subscribe(sql => {
      this.sqlitePorter.importSqlToDb(this.database, sql)
        .then(_ => {
          this.loadPoultry();
          this.loadConsumption();
          this.dbReady.next(true);
        })
        .catch(e => console.error(e));
    });
  }
 
  getDatabaseState() {
    return this.dbReady.asObservable();
  }
 
  getDevs(): Observable<Dev[]> {
    return this.poultry.asObservable();
  }
 
  getProducts(): Observable<any[]> {
    return this.consumption.asObservable();
  }


  loadPoultry() {
    return this.database.executeSql('SELECT * FROM poultry', []).then(data => {
      let poultry: Dev[] = [];
 
      if (data.rows.length > 0) {
        for (var i = 0; i < data.rows.length; i++) {
 
          poultry.push({ 
            poultryId: data.rows.item(i).poultryId,
            poultryName: data.rows.item(i).poultryName, 
            poultryCount: data.rows.item(i).poultryCount,
            poultryType: data.rows.item(i).poultryType,
            poultryDate: data.rows.item(i).poultryDate, 
            poultryNotes: data.rows.item(i).poultryNotes
           });
        }
      }
      this.poultry.next(poultry);
    });
  }

  addPoultry(poultryId, poultryName, poultryCount, poultryType, poultryDate, poultryNotes) {
    let data = [poultryId, poultryName, poultryCount , poultryType, poultryDate, poultryNotes];
    return this.database.executeSql('INSERT INTO poultry (poultryId, poultryName, poultryCount , poultryType, poultryDate, poultryNotes) VALUES (?, ?, ?, ?, ?, ?)', data).then(data => {
      this.loadPoultry();
    });
  }

  getPoultry(id): Promise<Dev> {
    return this.database.executeSql('SELECT * FROM poultry WHERE poultryId = ?', [id]).then(data => {
      return {
        poultryId: data.rows.item(0).poultryId,
        poultryName: data.rows.item(0).poultryName, 
        poultryCount: data.rows.item(0).poultryCount, 
        poultryType: data.rows.item(0).poultryType, 
        poultryDate: data.rows.item(0).poultryDate, 
        poultryNotes: data.rows.item(0).poultryNotes
      }
    });
  }

  deletePoultry(id) {
    return this.database.executeSql('DELETE FROM poultry WHERE poultryId = ?', [id]).then(_ => {
      this.loadPoultry();
      this.loadConsumption();
    });
  }

  updatePoultry(dev: Dev) {
    let data = [dev.poultryName, dev.poultryCount, dev.poultryType, dev.poultryDate, dev.poultryNotes];
    return this.database.executeSql(`UPDATE poultry SET poultryName = ?, poultryCount = ?, poultryType, poultryDate = ?, poultryNotes = ? WHERE poultryId = ${dev.poultryId}`, data).then(data => {
      this.loadPoultry();
    })
  }


  loadConsumption() {
    let query = 'SELECT * FROM consumption';
    return this.database.executeSql(query, []).then(data => {
      let consumption = [];
      if (data.rows.length > 0) {
        for (var i = 0; i < data.rows.length; i++) {
          consumption.push({ 
            consumptionId: data.rows.item(i).consumptionId,
            consumptionPoultry: data.rows.item(i).consumptionPoultry,
            consumptionDate: data.rows.item(i).consumptionDate,
            consumptionAmount: data.rows.item(i).consumptionAmount,
            consumptionFeed: data.rows.item(i).consumptionFeed,
           });
        }
      }
      this.consumption.next(consumption);
    });
  }

  addConsumption(consumptionId, consumptionPoultry, consumptionDate, consumptionAmount, consumptionFeed) {
    let data = [consumptionId, consumptionPoultry, consumptionDate, consumptionAmount, consumptionFeed];
    return this.database.executeSql('INSERT INTO consumption (consumptionId, consumptionPoultry, consumptionDate, consumptionAmount, consumptionFeed) VALUES (?, ?, ?, ?, ?)', data).then(data => {
      this.loadConsumption();
    });
  }




  
}

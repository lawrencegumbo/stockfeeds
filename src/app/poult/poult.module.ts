import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PoultPageRoutingModule } from './poult-routing.module';

import { PoultPage } from './poult.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PoultPageRoutingModule
  ],
  declarations: [PoultPage]
})
export class PoultPageModule {}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PoultPage } from './poult.page';

describe('PoultPage', () => {
  let component: PoultPage;
  let fixture: ComponentFixture<PoultPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PoultPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PoultPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { DatabaseService, Dev } from '../services/database.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-poult',
  templateUrl: './poult.page.html',
  styleUrls: ['./poult.page.scss'],
})
export class PoultPage implements OnInit {

  poult: Dev = null;
  consump = '';

  constructor(
    private route: ActivatedRoute, private db: DatabaseService, private router: Router, private toast: ToastController
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      let devId = params.get('id');
 
      this.db.getPoultry(devId).then(data => {
        this.poult = data;

      });
    });
  }

  delete() {
    this.db.getPoultry(this.poult.poultryId).then(() => {
      this.router.navigateByUrl('/');
    });
  }
 
  updateDeveloper() { 
    this.db.updatePoultry(this.poult).then(async (res) => {
      let toast = await this.toast.create({
        message: 'Developer updated',
        duration: 3000
      });
      toast.present();
    });
  }

}

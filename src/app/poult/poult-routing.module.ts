import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PoultPage } from './poult.page';

const routes: Routes = [
  {
    path: '',
    component: PoultPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PoultPageRoutingModule {}

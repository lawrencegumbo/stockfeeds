import { DatabaseService, Dev } from '../services/database.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-poultry',
  templateUrl: './poultry.page.html',
  styleUrls: ['./poultry.page.scss'],
})
export class PoultryPage implements OnInit {

  poultry: Dev[] = [];
 
  consumption: Observable<any[]>;
 
  poult = {};
  consump = {};
 
  selectedView = 'devs';

  constructor(
    private db: DatabaseService
  ) { }

  ngOnInit() {
    this.db.getDatabaseState().subscribe(rdy => {
      if (rdy) {
        this.db.getDevs().subscribe(devs => {
          this.poultry = devs;
        })
        this.consumption = this.db.getProducts();
      }
    });
  }

  addPoultry() { 
    this.db.addPoultry(this.poult['poultryId'], this.poult['poultryName'], this.poult['poultryCount'], this.poult['poultryType'], this.poult['poultryDate'], this.poult['poultryNotes'])
    .then(_ => {
      this.poult = {};
    });
  }

 
  addConsumption() {
    this.db.addConsumption(this.consump['consumptionId'], this.consump['consumptionPoultry'], this.consump['consumptionDate'], this.consump['consumptionAmount'], this.consump['consumptionFeed'])
    .then(_ => {
      this.consump = {};
    });
  }


}

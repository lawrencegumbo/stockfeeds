import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PoultryPageRoutingModule } from './poultry-routing.module';

import { PoultryPage } from './poultry.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PoultryPageRoutingModule
  ],
  declarations: [PoultryPage]
})
export class PoultryPageModule {}

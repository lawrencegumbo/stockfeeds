import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PoultryPage } from './poultry.page';

describe('PoultryPage', () => {
  let component: PoultryPage;
  let fixture: ComponentFixture<PoultryPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PoultryPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PoultryPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

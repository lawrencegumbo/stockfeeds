import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PoultryPage } from './poultry.page';

const routes: Routes = [
  {
    path: '',
    component: PoultryPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PoultryPageRoutingModule {}

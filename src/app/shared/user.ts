export interface User {
   uid: string;
   email: string;
   tel : string;
   displayName: string;
   photoURL: string;
   emailVerified: boolean;
}
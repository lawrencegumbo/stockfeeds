

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


CREATE TABLE `consumption` (
  `consumptionId` int(11) NOT NULL,
  `consumptionPoultry` int(6) NOT NULL,
  `consumptionDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `consumptionAmount` decimal(10,2) NOT NULL,
  `consumptionFeed` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



INSERT INTO `consumption` (`consumptionId`, `consumptionPoultry`, `consumptionDate`, `consumptionAmount`, `consumptionFeed`) VALUES
(1, 1, '2020-03-18 11:58:03', '250.50', 2),
(2, 2, '2020-03-18 11:58:14', '460.30', 1);



CREATE TABLE `expenses` (
  `expensesID` int(11) NOT NULL,
  `expensesDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `expensesPoultry` int(11) NOT NULL,
  `expensesAmount` decimal(10,2) NOT NULL,
  `expensesNotes` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



INSERT INTO `expenses` (`expensesID`, `expensesDate`, `expensesPoultry`, `expensesAmount`, `expensesNotes`) VALUES
(1, '2020-03-18 12:01:13', 1, '302.40', 'testing'),
(2, '2020-03-18 12:01:13', 2, '250.00', 'also testing');



CREATE TABLE `feed` (
  `feedID` int(11) NOT NULL,
  `feedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `feedAmount` decimal(10,2) NOT NULL,
  `feedName` varchar(255) NOT NULL,
  `feedPoultry` int(11) NOT NULL DEFAULT '0',
  `feedPhase` int(11) NOT NULL DEFAULT '0',
  `feedNotes` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



INSERT INTO `feed` (`feedID`, `feedDate`, `feedAmount`, `feedName`, `feedPoultry`, `feedPhase`, `feedNotes`) VALUES
(1, '2020-03-18 12:06:23', '5000.00', 'Phase 2', 1, 2, 'testing feeds'),
(2, '2020-03-18 12:06:23', '2000.00', 'Phase 1', 2, 1, 'testing');



CREATE TABLE `light` (
  `lightId` int(11) NOT NULL,
  `lightPoultry` int(6) NOT NULL,
  `lightIntensity` decimal(10,2) NOT NULL,
  `lightHours` int(3) NOT NULL,
  `lightDate` datetime NOT NULL,
  `lightNotes` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `medication` (
  `medicationId` int(11) NOT NULL,
  `medicationPoultry` int(6) NOT NULL,
  `medicationDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `medicationDose` decimal(10,2) NOT NULL,
  `medicationMed` int(3) NOT NULL,
  `medicationNotes` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `meds` (
  `medsId` int(2) NOT NULL,
  `medsName` varchar(50) NOT NULL,
  `medsImage` int(50) NOT NULL,
  `medsNotes` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE `mortality` (
  `mortalityId` int(11) NOT NULL,
  `mortalityPoultry` int(6) NOT NULL,
  `mortalityDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `mortalityNumber` int(5) NOT NULL,
  `mortalityNotes` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE `phase` (
  `phaseId` int(11) NOT NULL,
  `phaseName` varchar(50) NOT NULL,
  `phaseImage` varchar(100) NOT NULL,
  `phasePoultryType` int(2) NOT NULL,
  `phaseNotes` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO `phase` (`phaseId`, `phaseName`, `phaseImage`, `phasePoultryType`, `phaseNotes`) VALUES
(1, 'Phase 1', 'layer-phase1.jpg', 1, 'notes here'),
(2, 'Phase 2', 'layer-phase2.jpg', 1, 'notes here'),
(3, 'Phase 3', 'layer-phase3.jpg', 1, 'Notes here'),
(4, 'Phase 1', 'broiler-phase1.jpg', 2, 'Notes here'),
(5, 'Phase 2', 'broiler-phase2.jpg', 2, 'Notes Here'),
(6, 'Phase 3', 'broiler-phase3.jpg', 2, 'Notes here');



CREATE TABLE `poultry` (
  `poultryId` int(11) NOT NULL,
  `poultryName` varchar(255) NOT NULL,
  `poultryCount` int(6) NOT NULL,
  `poultryType` int(1) NOT NULL,
  `poultryDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `poultryNotes` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE `poultryType` (
  `poultryTypeID` int(11) NOT NULL,
  `poultryTypeName` varchar(255) NOT NULL,
  `poultryTypeImage` varchar(255) NOT NULL,
  `poultryTypeNotes` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



INSERT INTO `poultryType` (`poultryTypeID`, `poultryTypeName`, `poultryTypeImage`, `poultryTypeNotes`) VALUES
(1, 'Layer Chickens', 'layer-chickens.jpg', 'layer notes here'),
(2, 'Broiler Chickens', 'broiler-chickens.jpg', 'Broiler note here');



CREATE TABLE `sold` (
  `soldId` int(11) NOT NULL,
  `soldPoultry` int(6) NOT NULL,
  `soldDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `soldNumber` int(4) NOT NULL,
  `soldAmount` decimal(10,2) NOT NULL,
  `soldNotes` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `temperature` (
  `temperatureId` int(11) NOT NULL,
  `temperaturePoultry` int(5) NOT NULL,
  `temperatureDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `temperatureTemp` int(4) NOT NULL,
  `temperatureNotes` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE `weight` (
  `weightId` int(11) NOT NULL,
  `weightPoultry` int(6) NOT NULL,
  `weightDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `weightWeight` decimal(10,2) NOT NULL,
  `weightNotes` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `consumption`
  ADD PRIMARY KEY (`consumptionId`);


ALTER TABLE `expenses`
  ADD PRIMARY KEY (`expensesID`);


ALTER TABLE `feed`
  ADD PRIMARY KEY (`feedID`);

--
-- Indexes for table `light`
--
ALTER TABLE `light`
  ADD PRIMARY KEY (`lightId`);

--
-- Indexes for table `medication`
--
ALTER TABLE `medication`
  ADD PRIMARY KEY (`medicationId`);

--
-- Indexes for table `meds`
--
ALTER TABLE `meds`
  ADD PRIMARY KEY (`medsId`);

--
-- Indexes for table `mortality`
--
ALTER TABLE `mortality`
  ADD PRIMARY KEY (`mortalityId`);

--
-- Indexes for table `phase`
--
ALTER TABLE `phase`
  ADD PRIMARY KEY (`phaseId`);

--
-- Indexes for table `poultry`
--
ALTER TABLE `poultry`
  ADD PRIMARY KEY (`poultryId`);

--
-- Indexes for table `poultryType`
--
ALTER TABLE `poultryType`
  ADD PRIMARY KEY (`poultryTypeID`);

--
-- Indexes for table `sold`
--
ALTER TABLE `sold`
  ADD PRIMARY KEY (`soldId`);

--
-- Indexes for table `temperature`
--
ALTER TABLE `temperature`
  ADD PRIMARY KEY (`temperatureId`);

--
-- Indexes for table `weight`
--
ALTER TABLE `weight`
  ADD PRIMARY KEY (`weightId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `consumption`
--
ALTER TABLE `consumption`
  MODIFY `consumptionId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `expenses`
--
ALTER TABLE `expenses`
  MODIFY `expensesID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `feed`
--
ALTER TABLE `feed`
  MODIFY `feedID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `light`
--
ALTER TABLE `light`
  MODIFY `lightId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `medication`
--
ALTER TABLE `medication`
  MODIFY `medicationId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `meds`
--
ALTER TABLE `meds`
  MODIFY `medsId` int(2) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mortality`
--
ALTER TABLE `mortality`
  MODIFY `mortalityId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `phase`
--
ALTER TABLE `phase`
  MODIFY `phaseId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `poultry`
--
ALTER TABLE `poultry`
  MODIFY `poultryId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `poultryType`
--
ALTER TABLE `poultryType`
  MODIFY `poultryTypeID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sold`
--
ALTER TABLE `sold`
  MODIFY `soldId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `temperature`
--
ALTER TABLE `temperature`
  MODIFY `temperatureId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `weight`
--
ALTER TABLE `weight`
  MODIFY `weightId` int(11) NOT NULL AUTO_INCREMENT;
